package com.smartbill.server.service;

import com.smartbill.server.model.Consumer;
import com.smartbill.server.model.Product;
import com.smartbill.server.model.Purchase;
import com.smartbill.server.model.Purchase;

import java.util.List;

public interface PurchaseService {
    void create(Purchase purchase);
    List<Purchase> readAll();
   // List<Product> readProducts(int id);
   // List<Consumer> readConsumers(int id);
    Purchase read(int id);
    boolean update(Purchase purchase, int id);
    boolean addProducts(int id,  List<Product> products);
    boolean addConsumers(int id, List<Consumer> consumers);
    boolean delete(int id);
}
